package com.example.generisanjewebstranica.model.MetaData;

public class StraniKljuc {
    String PKTableName;
    String PKColumnName;
    String FKTableName;
    String FKColumnName;
    String FKName;

    public StraniKljuc(String pkt, String pkc, String fkt, String fkc, String fk) {
        PKTableName = pkt;
        PKColumnName = pkc;
        FKTableName = fkt;
        FKColumnName = fkc;
        FKName = fk;
    }

    public String getPKTableName() {
        return PKTableName;
    }

    public String getPKColumnName() {
        return PKColumnName;
    }

    public String getFKTableName() {
        return FKTableName;
    }

    public String getFKColumnName() {
        return FKColumnName;
    }

    public String getFKName() {
        return FKName;
    }
}


