package com.example.generisanjewebstranica.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.generisanjewebstranica.model.Html;
import com.example.generisanjewebstranica.nosql.model.HtmlNoSQL;
import com.example.generisanjewebstranica.nosql.repository.HtmlRepositoryNoSQL;
import com.example.generisanjewebstranica.repository.HtmlRepository;

/** Klasa predstavlja Service. Za realizaciju potrebnih funkcionalnosti koristi HtmlRepository.
 * @author Amera Alic
*/
@Service
public class HtmlService {
    @Autowired
    HtmlRepository htmlRepository;
    @Autowired
    HtmlRepositoryNoSQL htmlRepositoryNoSQL;

    /** Metoda implementira funkcionalnost pronalazenja svih Html objekata unutar baze podataka.
     * @return Kolekciju svih pronadjenih Html objekata.
    */
    public Iterable<Html> getHtml() {
        return htmlRepository.findAll();
    }

    /** Metoda implementira funkcionalnost pronalazenja Html objekta po id-u objekta.
     * @param id Html id.
     * @return Html ako je pronadjen, null ako nije.
    */
    public Optional<Html> getById(Integer id) {
        return htmlRepository.findById(id);
    }
    /**
     * Metoda implementira funkcionalnost prinalazenja svih Html objekata iz relacione i nerelacione baze podataka
     * Htmlove iz Mongo baze dodaje na one iz Oracle baze, koje smo premjestili u listu
     * @return svi Html objekti iz obje baze
     */
    public Iterable<Html> getAllHtml() {
    	Iterable<Html> all_html = htmlRepository.findAll();
    	Iterable<HtmlNoSQL> all_nosql_html = htmlRepositoryNoSQL.findAll();
    	List<Html> sviHTML=new ArrayList();
    	
    	for(HtmlNoSQL html : all_nosql_html) {
    		Html h=new Html();
    		h.setDatumHtml(html.getDatumHtml());
    		h.setOpisHtml(html.getOpisHtml());
    		h.setNazivHtml(html.getNazivHtml());
    		h.setHtmlKod(html.getHtmlKod());
    		h.setCss(null);
    		h.setKorisnik(null);
    		sviHTML.add(h);
    	}
    	all_html.forEach(sviHTML::add);
    	all_html=sviHTML;
    	return all_html;
    }
}
