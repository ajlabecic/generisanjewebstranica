package com.example.generisanjewebstranica.controller2;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


import com.example.generisanjewebstranica.service.CssService;

import java.util.Optional;

import com.example.generisanjewebstranica.model.Css;

/** Klasa predstavlja RestControlle. Implementira potrebne rute za manipulaciju podacima o css datotekama.
 * @author Ajla Becic
*/
@RestController
public class CssController {
    @Autowired
    CssService cssService;

    /** Ruta za dobavljanje svih css datoteka iz baze.
     * @return Objekat tipa Iterable<Css> koji predstavlja kolekciju svih css fajlova koji se nalaze u bazi podataka.
    */
    @GetMapping("/cssall")
    public Iterable<Css> getCssAll() {
        return cssService.getCss();
    }

    /** Ruta za dobavljanje jednog css objekta iz baze, na osnovu ID-a koji se salje kao parametar.
     * @param id Css id.
     * @return Css ako je pronadjen u bazi, null ako nije.
    */
    @GetMapping("/css")
    public Optional<Css> getCss(@RequestParam(value="id") Integer id) {
        return cssService.getById(id);
    }
}
