package com.example.generisanjewebstranica.nosql.model;

import javax.persistence.GeneratedValue;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/** Klasa koja implementira dokument u MongoDB bazi podataka
 * @author Ajla Becic
 */
@Document(collection = "css")
public class CssNoSql {
	
	@Id
    @GeneratedValue
	private String id;
	
	@Field("naziv_css")
	private String nazivCss;
	
	@Field("css_kod")
	private byte[] cssKod;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getNazivCss() {
		return nazivCss;
	}
	public void setNazivCss(String nazivCss) {
		this.nazivCss = nazivCss;
	}
	public byte[] getCssKod() {
		return cssKod;
	}
	public void setCssKod(byte[] cssKod) {
		this.cssKod = cssKod;
	}
	public CssNoSql() {
		
	}
	
}
