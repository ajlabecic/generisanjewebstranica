package com.example.generisanjewebstranica.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.generisanjewebstranica.model.Korisnik;

/** Interfejs predstavlja Repository. Koristeci CrudRepository moguce je pristupiti raznim funkcijama za manipulaciju nad bazom podataka.
 * @author Maid Bajramovic
*/
@Repository
public interface KorisnikRepository extends CrudRepository<Korisnik, Integer> {
}


