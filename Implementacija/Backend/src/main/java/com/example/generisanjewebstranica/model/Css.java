package com.example.generisanjewebstranica.model;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/** Klasa predstavlja Entity. Opisuje strukturu tabele CSS_TABELA u bazi podataka i atribute objekta Css.
 * @author Ajla Becic
*/
@Entity
@Table(name="CSS_TABELA")
public class Css {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CSS_SEQ")
    @SequenceGenerator(sequenceName = "css_seq", allocationSize = 1, initialValue=100, name = "CSS_SEQ")
    private Integer id;

    /** Predstavlja .css datoteku.
    */
    @Lob
    @Column(name = "css_kod", columnDefinition="BLOB")
    private byte[] cssKod;
    
    @Column(name = "naziv_css")
    private String nazivCss;
    
    /** Predstavlja id korisnika koji je importovao datoteku.
    */
    @ManyToOne
	 @JoinColumn(name="korisnik_id", nullable=false)
	 private Korisnik korisnik;

    public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getCssKod() {
		return cssKod;
	}

	public void setCssKod(byte[] cssKod) {
		this.cssKod = cssKod;
	}

	public String getNazivCss() {
		return nazivCss;
	}

	public void setNazivCss(String nazivCss) {
		this.nazivCss = nazivCss;
	}

	/** Kreira Css objekat sa null vrijednostima.
	*/
	public Css() {}


    
}
