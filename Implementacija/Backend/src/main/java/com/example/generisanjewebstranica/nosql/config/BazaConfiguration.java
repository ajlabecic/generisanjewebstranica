package com.example.generisanjewebstranica.nosql.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
/**
 * Implementiramo klasu BazaConfiguration koja vrsi postavljanje konfiguracija za povezivanje na Mongo bazu podataka
 * @author Amera Alic
 *
 */
@Configuration
@ComponentScan(basePackages = "com.example.generisanjewebstranica.nosql.services")
@EnableMongoRepositories(basePackages = "com.example.generisanjewebstranica.nosql.repository")
@EnableTransactionManagement
public class BazaConfiguration {

	@Bean
    public MongoClient mongo() {
		MongoClientURI uri = new MongoClientURI("mongodb+srv://alicamera:Ameraetf0603!@cluster0-bsnsj.mongodb.net/test?retryWrites=true");
        return new MongoClient(uri);
    }
 
    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), "bazePodataka");
    }
}
