package com.example.generisanjewebstranica.model.MetaData;

public class Procedura {
    String ime;
    short tip;

    public Procedura(String ime, short tip) {
        this.ime = ime;
        this.tip = tip;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public short getTip() {
        return tip;
    }

    public void setTip(short tip) {
        this.tip = tip;
    }

    public String getStringTip() {
        switch(tip) {
            case 1:
                return "Nepoznat rezultat procedure.";
            case 2:
                return "Procedura ne vraca nista.";
            case 3:
                return "Procedura vraca rezultat.";
            default:
                return "";
        }
    }

}