package com.example.generisanjewebstranica.nosql.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.generisanjewebstranica.nosql.model.CssNoSql;
import com.example.generisanjewebstranica.nosql.model.HtmlNoSQL;
import com.example.generisanjewebstranica.nosql.services.HtmlServiceNoSQL;


/**
 * Implementiramo klasu koja ce pozivati servis i rezultate prikazivati na zadanoj ruti
 * @author Amera Alic
 *
 */
@RestController
public class HtmlControllerNoSQL {
	
	@Autowired
	HtmlServiceNoSQL htmlServiceNoSQL;
	
	/**
	 * Implementiramo metodu koja poziva funkciju getHtml() iz servisa
	 * i na ruti /nosql_htmls prikazuje sve html dokumente iz baze podataka
	 * @return kolekcija "proba"
	 */
	@GetMapping("/nosql_htmls")
    public Iterable<HtmlNoSQL> get() {
        return htmlServiceNoSQL.getHtml();
    }
	
	/**
	 * Implemntiramo metodu koja se pozvati servis i na ruti /nosql_html prikazati pronadjeni dokumnet na osnovu
	 * proslijedjenog id-a
	 * @param id nekog reda
	 * @return red sa zadanim id-jem
	 */
	@GetMapping("/nosql_html")
    public Optional<HtmlNoSQL> getCss(@RequestParam(value="id")String id) {
        return htmlServiceNoSQL.getById(id);
    }
	/**
	 * Implemetiramo metodu koja ce pozvati servis za dodavanje novog reda u bazu podataka
	 */
	@GetMapping("/nosql_save_html")
    public void save() {
		htmlServiceNoSQL.save();
    }

}
