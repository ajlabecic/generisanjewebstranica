package com.example.generisanjewebstranica.nosql.config;

import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/** Klasa kojom se konfiguriše pristup Neo4j grafovskoj bazi podataka.
 * @author Maid Bajramovic
 */
@Configuration
@ComponentScan(basePackages = "com.example.generisanjewebstranica.nosql.services")
@EnableNeo4jRepositories(basePackages = "com.example.generisanjewebstranica.nosql.repository")
@EnableTransactionManagement
public class Neo4jConfiguration {

    @Bean
    public SessionFactory sessionFactory() {
        // with domain entity base package(s)
        return new SessionFactory(configuration(), "com.example.generisanjewebstranica.nosql.model");
    }

    @Bean
    public org.neo4j.ogm.config.Configuration configuration() {
        org.neo4j.ogm.config.Configuration configuration = new org.neo4j.ogm.config.Configuration.Builder()
                .uri("bolt://hobby-jchkflpnnngcgbkeckcpddbl.dbs.graphenedb.com:24786")
                .credentials("mbajramovic", "b.XHXB2Z8ZxQ5j.nYd3tmr3ydH8HXs8")
                .build();
        return configuration;
    }

	@Bean
	public Neo4jTransactionManager transactionManager() {
		return new Neo4jTransactionManager(sessionFactory());
	}
}