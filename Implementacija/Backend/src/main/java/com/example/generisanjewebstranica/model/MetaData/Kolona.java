package com.example.generisanjewebstranica.model.MetaData;

public class Kolona {
    String naziv;
    String tip;
    String velicina;

    public Kolona(String naziv, String tip, String velicina) {
        this.naziv = naziv;
        this.tip = tip;
        this.velicina = velicina;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getTip() {
        return tip;
    }

    public String getVelicina() {
        return velicina;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public void setVelicina(String velicina) {
        this.velicina = velicina;
    }
}