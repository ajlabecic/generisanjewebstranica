package com.example.generisanjewebstranica.nosql.model;

import java.util.Date;

import javax.persistence.GeneratedValue;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Klasa koja implementira dokumnet u Mongo bazi podataka, jer je Mongo dokumnet-orjentisana baza podataka
 * @author Amera Alic
 *
 */
@Document(collection = "proba")
public class HtmlNoSQL {
	/**
	 * Definisemo sve atribute koji se ce spemati u bazi
	 */
	@Id
    @GeneratedValue
	private String id;
	
	@Field("html_kod")
	private byte[] htmlKod;
	
	@Field("naziv_html")
	private String nazivHtml;
	
	@Field("opis_html")
	private String opisHtml;
	
	@Field("datum_html")
	private Date datumHtml;
	/**
	 * desinisemo sve getere i setere za navedene atribute
	 * @return String koji se odnosi na id
	 */
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public byte[] getHtmlKod() {
		return htmlKod;
	}
	public void setHtmlKod(byte[] htmlKod) {
		this.htmlKod = htmlKod;
	}
	
	public String getNazivHtml() {
		return nazivHtml;
	}
	public void setNazivHtml(String nazivHtml) {
		this.nazivHtml = nazivHtml;
	}
	
	public String getOpisHtml() {
		return opisHtml;
	}
	public void setOpisHtml(String opisHtml) {
		this.opisHtml = opisHtml;
	}
	
	public Date getDatumHtml() {
		return datumHtml;
	}

	public void setDatumHtml(Date datumHtml) {
		this.datumHtml = datumHtml;
	}

	
	public HtmlNoSQL() {
		
	}
	
}
