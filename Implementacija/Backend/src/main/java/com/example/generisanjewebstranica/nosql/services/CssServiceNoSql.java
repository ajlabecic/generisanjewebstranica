package com.example.generisanjewebstranica.nosql.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.generisanjewebstranica.nosql.model.CssNoSql;
import com.example.generisanjewebstranica.nosql.repository.CssRepositoryNoSql;

/** Klasa koja implementira servis za rad sa MongoDB bazom podataka
 * @author Ajla Becic
 */
@Service
public class CssServiceNoSql {
	
	@Autowired
	CssRepositoryNoSql cssRepositoryNoSql;
	
	/** Metoda koja implementira spremanje css dokumenta u bazu podataka
     */
	public void save() {
		CssNoSql css = new CssNoSql();
		css.setNazivCss("naziv");
		css.setCssKod(("W29iamVjdCBPYmplY3Rd").getBytes());
		cssRepositoryNoSql.insert(css);
	}
	
	/** Metoda koja vraća sve css dokumente iz baze podataka
     * @return dokumenti pronadjeni u bazu podataka
     */
	public Iterable<CssNoSql> getAllCss() {		
        return cssRepositoryNoSql.findAll();
    }
	
	/** Metoda koja vraća jednan dokument iz baze podataka
     * @param id - id dokumenta
     * @return dokument sa proslijeđenim id-em
     */
	public Optional<CssNoSql> getCssById(String id) {
        return cssRepositoryNoSql.findById(id);
    }

}
