package com.example.generisanjewebstranica.nosql.controller;

import java.util.Optional;

import com.example.generisanjewebstranica.nosql.model.KorisnikNoSQL;
import com.example.generisanjewebstranica.nosql.services.KorisnikServiceNoSql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/** Klasa koja implementira rute za rad sa Neo4j bazom podataka.
 * @author Maid Bajramović
 */
@RestController
public class KorisnikControllerNoSql {
    @Autowired
    KorisnikServiceNoSql korisnikService;

    /** Metoda poziva servis za dodavanje korisnika
     * @return informacija o uspješnosti akcije dodavanja
     */
    @GetMapping("/nosql_saveKorisnik")
    public int save() {
        korisnikService.save();
        return 1;
    }

    /** Metoda koja poziva servis za brisanje svih korisnika
     * @return /
     */
    @GetMapping("/nosql_deleteAll")
    public void brisiSveKorisnike() {
         korisnikService.brisiSveKorisnike();
    }

    /** Metoda koja poziva servis za prikaz svih korisnika
     * @return lista svih korisnika
     */
    @GetMapping("/nosql_korisnici")
    public Iterable<KorisnikNoSQL> get() {
        return korisnikService.getKorisnici();
    }

    /** Metoda koja poziva servis za prikaz jednog korisnika
     * @param id - idkorisnika 
     * @param depth - željeni broj čvorova (dubina grafa)
     * @return korisnik sa poslanim id-em i željenim brojem čvorova
     */
    @GetMapping("/nosql_korisnik")
    public Optional<KorisnikNoSQL> getKorisnik(@RequestParam(value="id")Long id, @RequestParam(value="depth")int depth) {
        return korisnikService.getKorisnik(id, depth);
    }
}