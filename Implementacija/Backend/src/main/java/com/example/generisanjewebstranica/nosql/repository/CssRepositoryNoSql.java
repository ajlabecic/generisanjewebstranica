package com.example.generisanjewebstranica.nosql.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.generisanjewebstranica.nosql.model.CssNoSql;

/**
 * @author Ajla Becic
 */
@Repository
public interface CssRepositoryNoSql extends MongoRepository<CssNoSql, String>{

}
