package com.example.generisanjewebstranica.nosql.services;

import java.util.Optional;

import com.example.generisanjewebstranica.nosql.model.KorisnikNoSQL;
import com.example.generisanjewebstranica.nosql.repository.KorisnikRepositoryNoSQL;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Klasa koja implementira servis za rad sa Neo4j bazom podataka
 * @author Maid Bajramović
 */
@Service
public class KorisnikServiceNoSql {

    @Autowired
    KorisnikRepositoryNoSQL korisnikRepositoryNoSQL;

    /** Metoda koja implementira spremanje korisnika u bazu podataka
     * @return /
     */
    public void save() {
        // prvi cvor
        KorisnikNoSQL maid = new KorisnikNoSQL();
        maid.setIme("Maid");
        maid.setPrezime("Bajramovic");
        maid.setMail("mbajramovi1@etf.unsa.ba");

        // drugi cvor
        KorisnikNoSQL ajla = new KorisnikNoSQL();
        ajla.setIme("Ajla");
        ajla.setPrezime("Becic");
        ajla.setMail("abecic1@etf.unsa.ba");
        
        // veza prvog i drugog
        maid.poznaje(ajla);

        // treci cvor
        KorisnikNoSQL amera = new KorisnikNoSQL();
        amera.setIme("Amera");
        amera.setPrezime("Alic");
        amera.setMail("aalic2@etf.unsa.ba");

        // veza drugog i treceg
        ajla.poznaje(amera);

        korisnikRepositoryNoSQL.save(maid);
        korisnikRepositoryNoSQL.save(ajla);
        korisnikRepositoryNoSQL.save(amera);
    }

    /** Metoda koja briše sve korisnike iz baze podataka
     * @return /
     */
    public void brisiSveKorisnike() {
         korisnikRepositoryNoSQL.deleteAll();;
    }

    /** Metoda koja vraća sve korisnike iz baze podataka
     * @return korisnici dodani u bazu podataka
     */
    public Iterable<KorisnikNoSQL> getKorisnici() {
        return korisnikRepositoryNoSQL.findAll();
    }

    /** Metoda koja vraća jednog korisnika iz baze podataka
     * @param id - id korisnika
     * @param depth - dubina grafa (željeni broj čvorova)
     * @return korisnik sa proslijeđenim id-em i željenim brojem čvorova
     */
    public Optional<KorisnikNoSQL> getKorisnik(Long id, int depth) {
        return korisnikRepositoryNoSQL.findById(id, depth);
    }
    
}