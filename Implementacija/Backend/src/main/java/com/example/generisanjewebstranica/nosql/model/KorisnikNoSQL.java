package com.example.generisanjewebstranica.nosql.model;

import java.util.ArrayList;
import java.util.List;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/** Klasa koja implementira entitet u Neo4j bazi podataka
 * @author Maid Bajramović
 */

@NodeEntity
public class KorisnikNoSQL {
    @Id
    @GeneratedValue
    private Long id;
    private String ime;
    private String prezime;
    private String mail;
    private String lozinka;

    @Relationship(type="POZNAJE")
    private List<KorisnikNoSQL> korisnici=new ArrayList<>();

    public KorisnikNoSQL() {}

    public Long getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getMail() {
        return mail;
    }

    public String getLozinka() {
        return lozinka;
    }

    public List<KorisnikNoSQL> getKorisnici() {
        return korisnici;
    }

    public void setId(Long id) {
        this.id = id;
    } 

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void setKorisnici(List<KorisnikNoSQL> korisnici) {
        this.korisnici = korisnici;
    }

    /** Metoda koja dodaje novi susjedni čvor (osobu koju poznaje)
     * @param korisnik - koji se dodaje
     * @return /
     */
    public void poznaje(KorisnikNoSQL k) {
        this.korisnici.add(k);
    }
}