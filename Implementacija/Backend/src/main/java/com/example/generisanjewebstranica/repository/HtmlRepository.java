package com.example.generisanjewebstranica.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.generisanjewebstranica.model.Html;

/** Interfejs predstavlja Repository. Koristeci CrudRepository moguce je pristupiti raznim funkcijama za manipulaciju nad bazom podataka.
 * @author Amera Alic
*/
@Repository
public interface HtmlRepository extends CrudRepository<Html, Integer> {
}
