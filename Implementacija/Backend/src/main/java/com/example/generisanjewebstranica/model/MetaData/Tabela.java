package com.example.generisanjewebstranica.model.MetaData;

import java.util.ArrayList;
import java.util.List;

public class Tabela {
    String naziv;
    List<Kolona> kolone;
    List<PrimarniKljuc> primarniKljucevi;
    List<Indeks> indeksi;

    public Tabela(String naziv) {
        this.naziv = naziv;
        kolone = new ArrayList<Kolona>();
        primarniKljucevi = new ArrayList<PrimarniKljuc>();
        indeksi = new ArrayList<Indeks>();
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public List<Kolona> getKolone() {
        return kolone;
    }

    public void setKolone(List<Kolona> kolone) {
        this.kolone = kolone;
    }

    public void addKolona(Kolona kolona) {
        kolone.add(kolona);
    }

    public List<PrimarniKljuc> getPrimarniKljucevi() {
        return primarniKljucevi;
    }

    public void addPrimarniKljuc(PrimarniKljuc kljuc) {
        primarniKljucevi.add(kljuc);
    }

    public List<Indeks> getIndeksi() {
        return indeksi;
    }

    public void addIndeks(Indeks ind) {
        indeksi.add(ind);
    }

}