package com.example.generisanjewebstranica.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.generisanjewebstranica.model.Korisnik;
import com.example.generisanjewebstranica.repository.KorisnikRepository;

import com.example.generisanjewebstranica.nosql.model.KorisnikNoSQL;
import com.example.generisanjewebstranica.nosql.repository.KorisnikRepositoryNoSQL;

/** Klasa predstavlja Service. Za realizaciju potrebnih funkcionalnosti koristi KorisnikRepository.
 * @author Maid Bajramovic
*/
@Service
public class KorisnikService {
    @Autowired
    KorisnikRepository korisnikRepository;
    @Autowired
    KorisnikRepositoryNoSQL korisnikRepositoryNoSQL;

    /** Metoda implementira funkcionalnost pronalazenja svih korisnika unutar baze podataka.
     * @return Kolekciju svih pronadjenih korisnika.
    */
    public Iterable<Korisnik> getKorisnike() {
        return korisnikRepository.findAll();
    }

    /** Metoda implementira funkcionalnost pronalazenja korisnika po id-u objekta.
     * @param id Korisnikov id.
     * @return Korisnika ako je pronadjen, null ako nije.
    */
    public Optional<Korisnik> getById(Integer id) {
        return korisnikRepository.findById(id);
    }
    
    /** Metoda implementira funkcionalnost spremanja korisnika u bazu podataka.
     * @param korisnik Korisnik kojeg je potrebno sporemiti u bazu.
    */
    public void spremi(Korisnik korisnik) {
        korisnikRepository.save(korisnik);
    }

    /** Metoda implementira funkcionalnost prikaza svih korisnika, iz obje baze podataka.
     * @return vraća sve korisnike sistema
     */
    public Iterable<Korisnik> povuciSveKorisnike() {
        Iterable<Korisnik> korisnici = korisnikRepository.findAll();
        Iterable<KorisnikNoSQL> korisniciNoSQL = korisnikRepositoryNoSQL.findAll();
        List<Korisnik> sviKorisnici = new ArrayList();

        for (KorisnikNoSQL korisnik : korisniciNoSQL) {
            Korisnik noSQLKorisnik = new Korisnik();
            noSQLKorisnik.setIme(korisnik.getIme());
            noSQLKorisnik.setPrezime(korisnik.getPrezime());
            noSQLKorisnik.setMail(korisnik.getMail());
            noSQLKorisnik.setLozinka(korisnik.getLozinka());
            sviKorisnici.add(noSQLKorisnik);
        }

        korisnici.forEach(sviKorisnici::add);
        korisnici = sviKorisnici;
        return korisnici;
        
    }
}