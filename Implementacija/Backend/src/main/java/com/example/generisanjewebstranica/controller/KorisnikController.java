package com.example.generisanjewebstranica.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;


import com.example.generisanjewebstranica.service.KorisnikService;

import java.util.Optional;

import com.example.generisanjewebstranica.model.Korisnik;


/** Klasa predstavlja RestControlle. Implementira potrebne rute za manipulaciju podacima o korisniku.
 * @author Maid Bajramovic
*/

@RestController
public class KorisnikController {
    @Autowired
    KorisnikService korisnikService;
    
    /** Ruta za dobavljanje svih korisnika iz baze.
     * @return Objekat tipa Iterable<Korisnik> koji predstavlja kolekciju svih korisnika koji se nalaze u bazi podataka.
    */
    @GetMapping("/korisnici")
    public Iterable<Korisnik> korisnici() {
        return korisnikService.getKorisnike();
    }

    /** Ruta za dobavljanje jednog korisnika iz baze, na osnovu ID-a koji se salje kao parametar.
     * @param id Korisnikov id.
     * @return Korisnika ako je pronadjen u bazi, null ako nije.
    */
    @GetMapping("/korisnik")
    public Optional<Korisnik> getKorisnik(@RequestParam(value="id") Integer id) {
        return korisnikService.getById(id);
    }
    
    /** Ruta za testiranje spremanja korisnika u bazu. Trenutno se ne koristi.
    */
    @GetMapping("/korisniksave")
    public void saveKorisnik() {
    	Korisnik korisnik = new Korisnik();
    	//korisnik.setId(77);
        korisnikService.spremi(korisnik);
    }

    /**
     * Ruta za prikaz svih korisnika (onih u Oracle i Neo4j bazi podataka.)
     * @return vraća sve korisnike
     */
    @GetMapping("/sviKorisnici")
    public Iterable<Korisnik> sviKorisnici() {
        return korisnikService.povuciSveKorisnike();
    }
}