package com.example.generisanjewebstranica.controller3;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


import com.example.generisanjewebstranica.service.HtmlService;

import java.util.Optional;

import com.example.generisanjewebstranica.model.Html;
import com.example.generisanjewebstranica.model.Korisnik;

/** Klasa predstavlja RestControlle. Implementira potrebne rute za manipulaciju podacima o html datotekama.
 * @author Amera Alic
*/
@RestController
public class HtmlController {
    @Autowired
    HtmlService htmlService;

    /** Ruta za dobavljanje svih html datoteka iz baze.
     * @return Objekat tipa Iterable<Html> koji predstavlja kolekciju svih html fajlova koji se nalaze u bazi podataka.
    */
    @GetMapping("/htmls")
    public Iterable<Html> getAllHtmls() {
        return htmlService.getHtml();
    }

    /** Ruta za dobavljanje jednog html objekta iz baze, na osnovu ID-a koji se salje kao parametar.
     * @param id Html id.
     * @return Html ako je pronadjen u bazi, null ako nije.
    */
    @GetMapping("/html")
    public Optional<Html> getCss(@RequestParam(value="id") Integer id) {
        return htmlService.getById(id);
    }
    /**
     * Ruta za prikaz svi Html objekata iz obje baze podataka
     * @return Svi objekti tipa Html i HtmlNoSQL
     */
    @GetMapping("/sviHtml")
    public Iterable<Html> sviHtml() {
        return htmlService.getAllHtml();
    }
}
