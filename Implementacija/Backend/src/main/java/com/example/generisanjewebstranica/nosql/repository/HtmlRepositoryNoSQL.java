package com.example.generisanjewebstranica.nosql.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.example.generisanjewebstranica.nosql.model.HtmlNoSQL;

/**
 * Repozitor poziva servis tj. pozvan je od strane servisa.
 * @author Amera Alic
 *
 */
@Repository
public interface HtmlRepositoryNoSQL extends MongoRepository<HtmlNoSQL, String>{

}
