package com.example.generisanjewebstranica.nosql.repository;

import java.util.List;

import com.example.generisanjewebstranica.nosql.model.KorisnikNoSQL;

import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Maid Bajramović
 */
@Repository
public interface KorisnikRepositoryNoSQL extends Neo4jRepository<KorisnikNoSQL, Long> {

}