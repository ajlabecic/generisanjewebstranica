package com.example.generisanjewebstranica.service;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.example.generisanjewebstranica.model.MetaData.Indeks;
import com.example.generisanjewebstranica.model.MetaData.Kolona;
import com.example.generisanjewebstranica.model.MetaData.PrimarniKljuc;
import com.example.generisanjewebstranica.model.MetaData.Procedura;
import com.example.generisanjewebstranica.model.MetaData.StraniKljuc;
import com.example.generisanjewebstranica.model.MetaData.Tabela;

import org.springframework.stereotype.Service;

@Service
public class MetaDataService {
    org.hibernate.engine.spi.SessionImplementor sessionImplementor;
    DatabaseMetaData metaData;

    @PersistenceContext
    private EntityManager em;

    private void initializeMetaData() {
        sessionImplementor = (org.hibernate.engine.spi.SessionImplementor) em.getDelegate();
        try {
            metaData = sessionImplementor.connection().getMetaData();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Tabela> getTables() {
        initializeMetaData();
        String[] types = {"TABLE"};
        try {
            List<Tabela> listaTabela = new ArrayList<>();
            ResultSet tabele = metaData.getTables(null, "BP17", "%", types);
            while(tabele.next()) {
                Tabela tabela = new Tabela(tabele.getString(3));
                
                ResultSet kolone = metaData.getColumns(null, "%", tabele.getString(3), "%");
                while(kolone.next()) {
                    Kolona kolona = new Kolona(kolone.getString(4), kolone.getString(6), kolone.getString(7));
                    tabela.addKolona(kolona);;
                }
                kolone.close();

                ResultSet primarniKljucevi = metaData.getPrimaryKeys(null, "BP17", tabela.getNaziv());
                while(primarniKljucevi.next()) {
                    tabela.addPrimarniKljuc(new PrimarniKljuc(primarniKljucevi.getString(4), primarniKljucevi.getString(6)));
                }
                primarniKljucevi.close();

                ResultSet indeksi = metaData.getIndexInfo(null, "BP17", tabela.getNaziv(), false, true);
                while(indeksi.next()) {
                    tabela.addIndeks(new Indeks(indeksi.getString(6), indeksi.getShort(7), indeksi.getString(9)));
                }
                indeksi.close();

                listaTabela.add(tabela);
            }
            tabele.close();
            return listaTabela;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<Procedura> getProcedure() {
        try {
            ResultSet procedure = metaData.getProcedures(null, "BP17", "%");
            List<Procedura> listaProcedura = new ArrayList<>();
            while(procedure.next()) {
            	listaProcedura.add(new Procedura(procedure.getString(3), procedure.getShort(8)));
            }
            return listaProcedura;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    public List<Procedura> getFunkcije() {
        try {
            ResultSet funkcije = metaData.getFunctions(null, "BP17", "%");
            List<Procedura> listaFunkcija = new ArrayList<>();
            while(funkcije.next()) {
                listaFunkcija.add(new Procedura(funkcije.getString(3), funkcije.getShort(8)));
            }
            return listaFunkcija;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<String> getTrigere() {
        try {
        	
        	
            ResultSet trigeri = metaData.getTables("%", "BP17", "%", new String[] {"TRIGGER"});
            List<String> listaTrigera = new ArrayList<>();
            
            while(trigeri.next()) {
                listaTrigera.add(trigeri.getString(3));
                 }
            return listaTrigera;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public List<StraniKljuc> getStraneKljuceve(List<Tabela> tabele) {
        try {
            List<StraniKljuc> listaStranihKljuceva = new ArrayList<>();
            int brTabela = tabele.size();
            for (int i = 0; i < brTabela; i++) {
                for (int j = 0; j < brTabela; j++) {
                    ResultSet straniKljucevi = metaData.getCrossReference(null, "BP17", tabele.get(i).getNaziv(), null, "BP17", tabele.get(j).getNaziv());
                    while(straniKljucevi.next()) {
                        listaStranihKljuceva.add(new StraniKljuc(straniKljucevi.getString(3), straniKljucevi.getString(4), straniKljucevi.getString(7), straniKljucevi.getString(8), straniKljucevi.getString(12)));
                    }
                    straniKljucevi.close();
                }
            }
            return listaStranihKljuceva;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}

