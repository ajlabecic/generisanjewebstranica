package com.example.generisanjewebstranica.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DB {
	//static String URL = "jdbc:mysql://localhost:3306/bazemigracija?useSSL=false&allowPublicKeyRetrieval=true";
	static String URL = "jdbc:mysql://localhost:3306/bazemigracija?useSSL=false&allowPublicKeyRetrieval=true";
	static String URL_NOSQL =  "jdbc:mysql://localhost:3306/nosql?useSSL=false&allowPublicKeyRetrieval=true";
	static String USERNAME= "root";
	static String PASSWORD ="";
	public static Connection getConnection() {
		Connection con=null;
		try {
			//Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(URL, USERNAME, PASSWORD);
			Statement stmt=con.createStatement();
			stmt=con.createStatement();
			
		}catch (Exception e){
			System.out.println(e);
			e.printStackTrace();	
		}
		return con;
	}

	public static Connection getConnectionForNoSQL() {
		Connection con=null;
		try {
			//Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection(URL_NOSQL, USERNAME, PASSWORD);
			Statement stmt=con.createStatement();
			stmt=con.createStatement();
			
		}catch (Exception e){
			System.out.println(e);
			e.printStackTrace();	
		}
		return con;
	}
}

