package com.example.generisanjewebstranica.model.MetaData;

public class PrimarniKljuc {
    String kolona;
    String naziv;

    public PrimarniKljuc(String kolona, String naziv) {
        this.kolona = kolona;
        this.naziv = naziv;
    }

    public String getKolona() {
        return kolona;
    }

    public String getNaziv() {
        return naziv;
    }
}