package com.example.generisanjewebstranica.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.generisanjewebstranica.connection.DB;
import com.example.generisanjewebstranica.connection.DBOracle;
import com.example.generisanjewebstranica.model.MetaData.Indeks;
import com.example.generisanjewebstranica.model.MetaData.Kolona;
import com.example.generisanjewebstranica.model.MetaData.Procedura;
import com.example.generisanjewebstranica.model.MetaData.StraniKljuc;
import com.example.generisanjewebstranica.model.MetaData.Tabela;
import com.example.generisanjewebstranica.nosql.model.CssNoSql;
import com.example.generisanjewebstranica.nosql.model.HtmlNoSQL;
import com.example.generisanjewebstranica.nosql.model.KorisnikNoSQL;
import com.example.generisanjewebstranica.nosql.services.CssServiceNoSql;
import com.example.generisanjewebstranica.nosql.services.HtmlServiceNoSQL;
import com.example.generisanjewebstranica.nosql.services.KorisnikServiceNoSql;
import com.mysql.jdbc.PreparedStatement;

@Service
public class MigracijaService {

	@Autowired
	KorisnikServiceNoSql korisnikServiceNoSql;
	@Autowired
	HtmlServiceNoSQL htmlServiceNoSQL;
	@Autowired
	CssServiceNoSql cssServiceNoSql;

	public Connection konekcijaNaMySQL() {
		Connection con = null;
		try {
			con = DB.getConnection();
			Statement stmt = con.createStatement();
			String sql = "use bazemigracija;";
			//String sql = "use test;";
			stmt.executeQuery(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;

	}

	public Connection konekcijaNaOracle() {
		Connection konekcija = null;
		try {
			konekcija = DBOracle.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return konekcija;

	}

	public void migracijaTabela(List<Tabela> tabele, Connection con) throws Exception {

		try {
			Statement stmt2 = con.createStatement();

			for (Tabela tabela1 : tabele) {
				String postoji = "drop table if exists " + tabela1.getNaziv();
				stmt2.execute(postoji);

				String sql2 = "create table " + tabela1.getNaziv() + " (";
				for (Kolona k : tabela1.getKolone()) {
					String tip = k.getTip();
					if (tip.equals("NUMBER"))
						tip = "INT";
					if (tip.equals("VARCHAR2"))
						tip = "VARCHAR";
					if (tip.equals("TIMESTAMP(6)"))
						tip = "VARCHAR";
					if (tip.equals("BLOB"))
						tip = "VARCHAR"; 
					sql2 += " " + k.getNaziv() + " " + tip + " (" + (Integer.parseInt(k.getVelicina()) + 20) + ") ";
					if (tabela1.getPrimarniKljucevi().size() != 0
							&& k.getNaziv().equals(tabela1.getPrimarniKljucevi().get(0).getKolona())) {
						sql2 += " primary key ";
					}
					sql2 += ", ";
				}
				sql2 = sql2.substring(0, sql2.length() - 2);
				sql2 += " );";
				System.out.println(sql2);
				stmt2.execute(sql2);

			}
			System.out.println("Kraj petlje");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void migracijaPodataka(List<Tabela> tabele, Connection con, Connection konekcija) throws Exception {
		try {
			Statement stmt3 = konekcija.createStatement();
			Statement stmt2 = con.createStatement();
			ResultSet podaci = null;

			for (Tabela tabela1 : tabele) {
				
				
				String dohvatiPodatke = "select * from " + tabela1.getNaziv();
				podaci = stmt3.executeQuery(dohvatiPodatke);

				ArrayList<String> getaj = new ArrayList<String>();
				for (Kolona k : tabela1.getKolone()) {
					String tip = k.getTip();
					if (tip.equals("NUMBER"))
						getaj.add("getInt");
					if (tip.equals("VARCHAR2"))
						getaj.add("getString");
					if (tip.equals("TIMESTAMP(6)"))
						getaj.add("getDate");
					if (tip.equals("BLOB"))
						getaj.add("getBlob");

				}

				while (podaci.next()) {
					
					String sql3 = "insert into " + tabela1.getNaziv() + " VALUES (";
					for (int i = 1; i < getaj.size() + 1; i++) {
						if (getaj.get(i - 1).equals("getInt"))
							sql3 += podaci.getInt(i);
						if (getaj.get(i - 1).equals("getString"))
							sql3 += ("'" + podaci.getString(i) + "'");
						if (getaj.get(i - 1).equals("getDate"))
							sql3 += ("'" + podaci.getString(i) + "'");
						if (getaj.get(i - 1).equals("getBlob"))
							sql3 += "'BlobFIle'";

						sql3 += ",";

					}
					//if(tabela1.getNaziv().equals("KORISNIK12")) sql3+="jfbdjfjdfj";
					
					sql3 = sql3.substring(0, sql3.length() - 1);
					sql3 += ");";
					System.out.println(sql3);
					stmt2.execute(sql3);
				}

			}

			System.out.println("Kraj petlje za podatke");

		} catch (Exception e) {
			throw(e);
		}

	}

	public void nosql_migracija(Connection konekcija) throws Exception {
		Iterable<KorisnikNoSQL> korisnici = korisnikServiceNoSql.getKorisnici();
		Iterable<HtmlNoSQL> htmls = htmlServiceNoSQL.getHtml();
		Iterable<CssNoSql> css = cssServiceNoSql.getAllCss();

		List<KorisnikNoSQL> _korisnici = new ArrayList<KorisnikNoSQL>();
		try {
			java.sql.PreparedStatement statement = null;
			String query = "";
			for (KorisnikNoSQL korisnik : korisnici) {
				query = "INSERT INTO korisnici (id, ime, prezime, mail, lozinka) VALUES (?,?,?,?,?)";
				statement = konekcija.prepareStatement(query);
				statement.setLong(1, korisnik.getId());
				statement.setString(2, korisnik.getIme());
				statement.setString(3, korisnik.getPrezime());
				statement.setString(4, korisnik.getMail());
				statement.setString(5, korisnik.getLozinka());
				statement.execute();
			}
			statement = null;
			for (HtmlNoSQL html : htmls) {
				query = "INSERT INTO htmls (id, htmlkod,nazivhtml, opishtml,datum) values (?,?,?,?,?)";
				statement = konekcija.prepareStatement(query);
				statement.setString(1, html.getId());
				statement.setBytes(2, html.getHtmlKod());
				statement.setString(3, html.getNazivHtml());
				statement.setString(4, html.getOpisHtml());
				statement.setString(5, html.getDatumHtml().toString());;
				statement.execute();
			}
			statement = null;
			for (CssNoSql c : css) {
				query = "insert into css (id,csskod, nazivcss) values (?,?,?)";
				statement = konekcija.prepareStatement(query);
				statement.setString(1, c.getId());
				statement.setBytes(2, c.getCssKod());
				statement.setString(3, c.getNazivCss());
				statement.execute();
			}
			konekcija.commit();
		}
		catch(Exception ex) {
			throw(ex);
		}
	}

	public void migracijaStranihKljuceva(List<StraniKljuc> straniKljucevi, Connection con) {
		try {
			Statement stmt2 = con.createStatement();
			for (StraniKljuc sk : straniKljucevi) {
				String sqlSk = "ALTER TABLE " + sk.getFKTableName() + " ADD FOREIGN KEY " + "(" + sk.getFKColumnName()
						+ ")" + " REFERENCES " + sk.getPKTableName() + "(" + sk.getPKColumnName() + ");";
				System.out.println(sqlSk);
				// stmt2.execute(sqlSk);
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public void migracijaTrigera(Connection con, Connection konekcija) {
		try {
			Statement stmt3 = konekcija.createStatement();
			Statement stmt2 = con.createStatement();
			String novi = "SELECT * FROM USER_TRIGGERS";
			ResultSet rsnovi = null;
			rsnovi = stmt3.executeQuery(novi);

			while (rsnovi.next()) {
				Statement stmt5 = con.createStatement();
				String postoji = "DROP TRIGGER IF EXISTS " + rsnovi.getString("TRIGGER_NAME");
				stmt2.execute(postoji);
				String sql4 = "CREATE TRIGGER " + rsnovi.getString("DESCRIPTION").toUpperCase() + "BEGIN \n";
				String body = rsnovi.getString("TRIGGER_BODY").toUpperCase();
				body = body.replaceAll("BEGIN", "");
				sql4 += body;
				sql4 = sql4.substring(0, sql4.length() - 1);
				sql4 += ";";
				String brisi = ":";
				sql4 = sql4.replaceAll(brisi, "");
				sql4 = sql4.replaceAll("NUMBER", "INT");
				sql4 = sql4.replaceAll("RAISE.*",
						"SIGNAL SQLSTATE '45000'\n" + "SET MESSAGE_TEXT = 'Order No not found in orders table';\r\n");
				System.out.println(sql4);
				stmt5.execute(sql4);

			}

		} catch (Exception e) {
			
					e.printStackTrace();

		}
	}

	public void migracijaProcedura(List<Procedura> procedure, Connection con, Connection konekcija) {
		try {
			ResultSet rsFun = null;
			Statement stmt3 = konekcija.createStatement();
			Statement stmt2 = con.createStatement();
			for (Procedura p : procedure) {
				/*
				 * String
				 * nazivFunkcije="select * from user_objects where object_type = 'FUNCTION'";
				 * ResultSet rsFun=stmt3.executeQuery(nazivFunkcije); String bodyFunkcije="";
				 * while(rsFun.next()) { bodyFunkcije="select text from user_source\r\n" +
				 * "where name = '" +rsFun.getString(1) + "' \norder by line";
				 * 
				 * }
				 */
				String bodyFunkcije = "";
				bodyFunkcije = "select text from user_source\r\n" + "where name = '" + p.getIme() + "' \norder by line";
				bodyFunkcije = bodyFunkcije.substring(0, bodyFunkcije.length());
				rsFun = stmt3.executeQuery(bodyFunkcije);
				String kodFunkcije = "";
				String ccc = "";
				String create = "CREATE FUNCTION ";
				String varijabla = "";
				String tip = "";
				int tijelo = 0;
				while (rsFun.next()) {
					kodFunkcije = rsFun.getString(1);
					ccc+=rsFun.getString(1);
					if (kodFunkcije.contains("FUNCTION")) {
						System.out.println(kodFunkcije.toString());
						String nazivFunkcije = kodFunkcije.substring(8, kodFunkcije.length()-1);
						System.out.println(nazivFunkcije);
						int i = 0;
						while(nazivFunkcije.charAt(i) != '(')
							i++;
						nazivFunkcije = kodFunkcije.substring(9,i + 8);
						create += nazivFunkcije ;
						kodFunkcije = kodFunkcije.substring(i+8);
						System.out.println(kodFunkcije);
						String parametri[] = kodFunkcije.split(",");
						for (int j = 0; j < parametri.length;j++) {
							i = 1;
							while (parametri[j].charAt(i) != ' ')
								i++;
							create += parametri[j].substring(0,i) +" ";
							
							create +="VARCHAR(70),";
						}
						create = create.substring(0, create.length()-1);
						create+=")";// create.substring(0,create.length()-2);
						System.out.println(create);
						
					}
					else if (kodFunkcije.contains("RETURN (")) {
						tijelo = 0;
						create += tip + " ";
						create += " RETURN " + varijabla + "; END ";
					}
					else if(kodFunkcije.contains("RETURN")) {
						create += " RETURNS ";
						int i = 0;
						while(kodFunkcije.charAt(i) != ' ')
							i++;
						i++;
						int j = i;
						while(kodFunkcije.charAt(i) != ' ')
							i++;
						tip = kodFunkcije.substring(j,i);
						if(tip.contains("NUMBER"))
							tip = "INTEGER";
						create += tip;
						i++;
						while(kodFunkcije.charAt(i) != ' ') // IS
							i++;
						i++;
						j = i;
						while(kodFunkcije.charAt(i) != ' ')
							i++;
						varijabla = kodFunkcije.substring(j,i);
						create += " DETERMINISTIC ";
					}
					else if(kodFunkcije.contains("BEGIN")) {

						create += "BEGIN DECLARE " + varijabla + " " + tip +";";
						tip = "";
						tijelo = 1;
					}
					else if(tijelo == 1) {
						tip += rsFun.getString(1);
					}
					System.out.println(create);
					System.out.println(varijabla);
					System.out.println(tip);
				}
				System.out.println(ccc);
				 stmt2.execute(create);

			}
		} catch (Exception e) {
			
			e.printStackTrace();

		}
	}

	public void migracijaIndeksa(List<Tabela> tabele, Connection con) {
		try {
			Statement stmt2 = con.createStatement();
			List<Indeks> indeksi = new ArrayList<Indeks>();
			String sqlI = "";
			for (Tabela tabela1 : tabele) {
				indeksi = tabela1.getIndeksi();
				for (Indeks i : indeksi) {
					String nazivIndeksa = i.getNaziv();
					String naziv_tabele = tabela1.getNaziv();
					String naziv_kolone = i.getKolona();
					sqlI = "CREATE INDEX " + nazivIndeksa + " ON " + naziv_tabele + " (" + naziv_kolone + ");";
					// stmt2.execute(sqlI);
				}
				System.out.println(sqlI);
			}
		} catch (Exception e) {
			
			e.printStackTrace();

		}
	}
}
