package com.example.generisanjewebstranica.model;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.SequenceGenerator;

/** Klasa predstavlja Entity. Opisuje strukturu tabele KORISNICI u bazi podataka i atribute objekta Korisnik.
 * @author Maid Bajramović
*/
@Entity
@Table(name="KORISNICI")
public class Korisnik {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KOR_SEQ")
    @SequenceGenerator(sequenceName = "korisnici_seq", allocationSize = 1, initialValue=100, name = "KOR_SEQ")
    private Integer id;

    @Column(name = "ime")
    private String ime;

    @Column(name = "prezime")
    private String prezime;

    @Column(name = "mail")
    private String mail;

    @Column(name = "lozinka")
    private String lozinka;

    /** Kreira Css objekat sa null vrijednostima.
	*/
    public Korisnik() {}


    public Integer getId() {
        return id;
    }

    public String getIme() {
        return ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public String getMail() {
        return mail;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

}