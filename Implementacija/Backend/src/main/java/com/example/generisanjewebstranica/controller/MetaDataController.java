package com.example.generisanjewebstranica.controller;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.generisanjewebstranica.connection.DB;
import com.example.generisanjewebstranica.connection.DBOracle;
import com.example.generisanjewebstranica.model.MetaData.Indeks;
import com.example.generisanjewebstranica.model.MetaData.Kolona;
import com.example.generisanjewebstranica.model.MetaData.Procedura;
import com.example.generisanjewebstranica.model.MetaData.StraniKljuc;
import com.example.generisanjewebstranica.model.MetaData.Tabela;
import com.example.generisanjewebstranica.service.MetaDataService;
import com.example.generisanjewebstranica.service.MigracijaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class MetaDataController {
    @Autowired
    MetaDataService metaDataService;
    @Autowired
    MigracijaService migracija;

    @RequestMapping(value="/metadata", method=RequestMethod.GET)
    public String getMetaData(Model model) {
    	Connection con=null;
    	Savepoint svpt1=null;
    	try  {
    		
            List<Tabela> tabele = metaDataService.getTables();
            List<Procedura> procedure = metaDataService.getProcedure();
            //List<Procedura> funkcije = metaDataService.getFunkcije();
            //List<String> trigeri = metaDataService.getTrigere();
            //List<StraniKljuc> straniKljucevi = metaDataService.getStraneKljuceve(tabele);

            //model.addAttribute("tabele", tabele);
            //model.addAttribute("procedure", procedure);
            //model.addAttribute("funkcije", funkcije);
            //model.addAttribute("trigeri", trigeri);
            //model.addAttribute("straniKljucevi", straniKljucevi);
            
            con=DB.getConnection();
            Connection konekcija=DBOracle.getConnection();
            
            con.setAutoCommit(false);
            Statement stmt2=con.createStatement();
            stmt2.execute("SET @@global.autocommit= 1;");
           
            //migracija.migracijaTabela(tabele,con);
            //migracija.migracijaIndeksa(tabele, con);
            //migracija.migracijaStranihKljuceva(straniKljucevi, con);
            //migracija.migracijaTrigera(con, konekcija);
            migracija.migracijaProcedura(procedure, con, konekcija);
            stmt2.execute("SET @@global.autocommit= 0;");
            svpt1=con.setSavepoint("SavePoint1");
            stmt2.execute("START TRANSACTION;");
            //migracija.migracijaPodataka(tabele,con, konekcija);
            con.commit();
            //stmt2.execute("commit;");
    	}catch(Exception e) {
    		if(con!=null) {
    			try {
    				Statement stmt2=con.createStatement();
            		con.rollback(svpt1);
            		System.out.println("ovdje i trbalo da uhvati baceni throw");
    			}catch(Exception e1) {
    				
    			}
    			
    		}
    		
    		
        	e.printStackTrace();
        }
        

        return "metadata";
    }
    
    @RequestMapping(value="nosql_to_sql", method=RequestMethod.GET)
    @ResponseBody
    public String nosqlToSql(){
        Connection konekcija = null;
        Savepoint savepoint = null;
        try {
            konekcija = DB.getConnectionForNoSQL();
            savepoint = konekcija.setSavepoint("sp1");
            konekcija.setAutoCommit(false);
            Statement statement = konekcija.createStatement();
            statement.execute("SET @@global.autocommit= 0;");
           
            statement.execute("START TRANSACTION;");
            migracija.nosql_migracija(konekcija);
            konekcija.commit();
            return "Završeno!";
        }
        catch(Exception e) {
            try {
                konekcija.rollback(savepoint);
                return e.getMessage();
            }
            catch(Exception ex) {
                ex.printStackTrace();
                return ex.getMessage();
            }

        }
    }

}