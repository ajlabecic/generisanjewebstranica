package com.example.generisanjewebstranica.model.MetaData;

public class Indeks {
    String naziv;
    short tip;
    String kolona;

    public Indeks(String naziv, short tip, String kolona) {
        this.naziv = naziv;
        this.tip = tip;
        this.kolona = kolona;
    }

    public String getNaziv() {
        return naziv;
    }

    public short getTip() {
        return tip;
    }

    public String getStringTip() {
        switch(tip) {
            case 1:
                return "tableIndexStatistic";
            case 2:
                return "tableIndexClustered";
            case 3:
                return "tableIndexHashed";
            case 4:
                return "tableIndexOther";
            default:
                return "";
        }
    }

    public String getKolona() {
        return kolona;
    }
}