package com.example.generisanjewebstranica.model;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/** Klasa predstavlja Entity. Opisuje strukturu tabele CSS_TABLE u bazi podataka i atribute objekta Html.
 * @author Amera Alic
*/
@Entity
@Table(name="HTML_TABLE")
public class Html {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HTML_SEQ")
    @SequenceGenerator(sequenceName = "html_seq", allocationSize = 1, initialValue=100, name = "HTML_SEQ")
    private Integer id;

    /** Predstavlja .html datoteku.
     */
    @Lob
    @Column(name = "html_kod", columnDefinition="BLOB")
    private byte[] htmlKod;
    
    @Column(name = "naziv_html")
    private String nazivHtml;
    
    @Column(name = "opis_html")
    private String opisHtml;
    
    @Column(name = "datum_html")
    private Date datumHtml;
    
    /** Predstavlja id korisnika koji je importovao datoteku.
     */
    @ManyToOne
	 @JoinColumn(name="korisnik_id", nullable=false)
	 private Korisnik korisnik;

    public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getHtmlKod() {
		return htmlKod;
	}

	public void setHtmlKod(byte[] htmlKod) {
		this.htmlKod = htmlKod;
	}

	public String getNazivHtml() {
		return nazivHtml;
	}

	public void setNazivHtml(String nazivHtml) {
		this.nazivHtml = nazivHtml;
	}
	
	public String getOpisHtml() {
		return opisHtml;
	}

	public void setOpisHtml(String opisHtml) {
		this.opisHtml = opisHtml;
	}
	
	public Date getDatumHtml() {
		return datumHtml;
	}

	public void setDatumHtml(Date datumHtml) {
		this.datumHtml = datumHtml;
	}

	
	@ManyToOne
	 @JoinColumn(name="css_id", nullable=false)
	 private Css css;
   public Css getCss() {
		return css;
	}

	public void setCss(Css css) {
		this.css = css;
	}
	
	/** Kreira Html objekat sa null vrijednostima atributa.
	*/
	public Html() {}


    
}
