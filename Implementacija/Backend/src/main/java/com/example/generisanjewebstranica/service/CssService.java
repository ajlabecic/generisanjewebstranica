package com.example.generisanjewebstranica.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import com.example.generisanjewebstranica.model.Css;
import com.example.generisanjewebstranica.repository.CssRepository;

/** Klasa predstavlja Service. Za realizaciju potrebnih funkcionalnosti koristi CssRepository.
 * @author Ajla Becic
*/
@Service
public class CssService {
    @Autowired
    CssRepository cssRepository;

    /** Metoda implementira funkcionalnost pronalazenja svih Css objekata unutar baze podataka.
     * @return Kolekciju svih pronadjenih Css objekata.
    */
    public Iterable<Css> getCss() {
        return cssRepository.findAll();
    }

    /** Metoda implementira funkcionalnost pronalazenja Css objekta po id-u objekta.
     * @param id Css id.
     * @return Css ako je pronadjen, null ako nije.
    */
    public Optional<Css> getById(Integer id) {
        return cssRepository.findById(id);
    }
}
