package com.example.generisanjewebstranica.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.generisanjewebstranica.model.Css;

/** Interfejs predstavlja Repository. Koristeci CrudRepository moguce je pristupiti raznim funkcijama za manipulaciju nad bazom podataka.
 * @author Ajla Becic
*/
@Repository
public interface CssRepository extends CrudRepository<Css, Integer> {
}
