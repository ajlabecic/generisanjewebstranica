package com.example.generisanjewebstranica.nosql.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.generisanjewebstranica.nosql.model.CssNoSql;
import com.example.generisanjewebstranica.nosql.services.CssServiceNoSql;

/** Klasa koja implementira rute za rad sa MongoDB bazom podataka.
 * @author Ajla Becic
 */
@RestController
public class CssControllerNoSql {
	
	@Autowired
	CssServiceNoSql cssServiceNoSql;
	
	/** Metoda poziva servis za prikaz svih css dokumenata
     * @return kolekcija svih css dokumenata koji se nalaze u bazi
     */
	@GetMapping("/nosql_allcss")
    public Iterable<CssNoSql> get() {
        return cssServiceNoSql.getAllCss();
    }
	
	/** Metoda poziva servis za pronalazak jedng css dokumenta u bazi(po id-u)
     * @return pronadjeni css dokument
     */
	@GetMapping("/nosql_css")
    public Optional<CssNoSql> getCss(@RequestParam(value="id")String id) {
        return cssServiceNoSql.getCssById(id);
    }
	
	/** Metoda poziva servis za dodavanje css dokumenta u Mongodb
     */
	@GetMapping("/nosql_savecss")
    public void save() {
        cssServiceNoSql.save();
    }

}
