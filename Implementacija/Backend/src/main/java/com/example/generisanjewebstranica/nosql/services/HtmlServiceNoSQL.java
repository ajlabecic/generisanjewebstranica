package com.example.generisanjewebstranica.nosql.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.generisanjewebstranica.nosql.model.HtmlNoSQL;
import com.example.generisanjewebstranica.nosql.repository.HtmlRepositoryNoSQL;

/**
 * Implementiramo servis za rad sa Mongo bazom dokumenta
 * i ovaj pozvan je od strane controllera, a poziva repozitorij.
 * @author Amera Alic
 *
 */

@Service
public class HtmlServiceNoSQL {
	
	@Autowired
	HtmlRepositoryNoSQL htmlRepositoryNoSQL;
	
	/**
	 * Implementiramo metodu koje se izvrsavati spasavanje html dokumenata u bazu podataka
	 */
	public void save() {
		HtmlNoSQL html = new HtmlNoSQL();
		html.setDatumHtml(new Date());
		html.setHtmlKod(("neki html kod").getBytes());
		html.setNazivHtml("Naziv html stranice ili dokumneta");
		html.setOpisHtml("opis dokumenta ili stranice na koju se odnosi");
		
		htmlRepositoryNoSQL.insert(html);
		
	}
	
	/**
	 * Implementiramo metodu koja se pronaci sve html dokumnete iz baze podatka
	 * @return objekat sa svim html dokumentima
	 */
	public Iterable<HtmlNoSQL> getHtml() {		
        return htmlRepositoryNoSQL.findAll();
    }
	
	/**
	 * Implementiramo metodu koja se vratiti tacno jedan dokument, na osnovu proslijedjenog id-a
	 * @param id se odnosi na id html dokumneta tj. id reda u kojem se nalazi
	 * @return objekat sa tacno jednim html dokumentom
	 */
	public Optional<HtmlNoSQL> getById(String id) {
        return htmlRepositoryNoSQL.findById(id);
    }

}
