package com.example.generisanjewebstranica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.generisanjewebstranica.service.CssService;
import com.example.generisanjewebstranica.service.HtmlService;
import com.example.generisanjewebstranica.service.KorisnikService;

import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class DemoApplication  {

	@Autowired
	KorisnikService korisnikService;
	@Autowired
	CssService cssService;
	
	@Autowired
	HtmlService htmlService;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


}
